package model

import (
	"reflect"
)

type IEntityModel interface {
	GetById(uint16) (IEntity, error)

	GetAll() ([]IEntity, error)

	GetByFilter(map[string]interface{}) ([]IEntity, error)
	
	CreateNew() (IEntity, error)
	
	CreateNewWithDefaults(interface{}) (IEntity, error)
	
	Save(IEntity) (error)
	
	SaveMany([]IEntity) (error)
}

type IEntitySchemaModel interface {
	IEntityModel
	
	GetRelatedEntityByName(string) (ISchemaEntity, error)
	GetRelatedEntityByType(reflect.Type) (ISchemaEntity, error)
}
