package model

// Base interface for 'entities' in this 
// shared (code) universe.
// 
// The entity concept comes from what was implemented 
// by the Drupal developers.
//
// While there are no declared methods, that 
// could change in the future.
type IEntity interface {
}

// HasArrayOfEntities is the interface that wraps
// two methods:
//
// ArrayOf returns an instance of an IEntity-implementation 
// casted as an interface{}.
//
// Entities returns an array of IEntity implementation instances
//
// This is useful for 1-to-many (hasMany) relationships, 
// like a CD has 1..N tracks
type HasArrayOfEntities interface {
	ArrayOf() interface{}
	Entities() []IEntity
}

// DEPRECATE (will remove soon)
type HasEntities interface {
	Entities() []IEntity
}
