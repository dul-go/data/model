# Data Modelling

## Installing to local Go environment

See this page:  
https://edenmal.moe/post/2017/Golang-go-get-from-Gitlab/  
  
```sh
git config --global url."git@gitlab.oit.duke.edu:".insteadOf "https://gitlab.oit.duke.edu/"
```

## Getting Started

This README will assume the reader has some working knowledge of the Go language, although the example(s) below can be seen as a primer of sorts.  

### Basic Premise
With the idea that a SQL-based database exists with a table such as:  
```sql
CREATE TABLE IF NOT EXISTS SYSADMIN (
    id INT PRIMARY KEY AUTOINCREMENT,
    first_name VARCHAR(35) NOT NULL,
    last_name VARCHAR(45) NOT NULL,
    active TINYINT(1)
)
```
Let's create a Go struct leveraging Go's "Tagging" feature (inside a typical project file)...  
```go
package main

import (
    "fmt"
)

type Sysadmin struct {
    Id int64 `sysadmin:"id"`
    
    FirstName string `sysadmin:"first_name"`
    
    LastName string `sysadmin:"last_name"`
    
    Active bool `sysadmin:"active"`
}

...
```