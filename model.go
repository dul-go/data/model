package model

import (
	"database/sql"
)

type IDbEntityModel interface {
	EntitiesPerPage() int
}

type DbAwareModel struct {
	IDbEntityModel
	
	TargetEntity IEntity
	
	database *sql.DB
}

func NewDbAwareModelFor(target IEntity, db *sql.DB) (*DbAwareModel, error) {
	return &DbAwareModel{TargetEntity: target, database: db}, nil
}

func (d *DbAwareModel) Db() (*sql.DB) {
	return d.database
}
