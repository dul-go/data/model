package model

import (
	"database/sql"
)

/*
	'ISchemaEntity' refers to an entity that can be
	mapped to a data schema, such as, but not limited to,
	a SQL database (Maria).

	For example, let's say Customer is a table in a database:
	CREATE TABLE customer (
		id INT PRIMARY KEY AUTOINCREMENT,
		name VARCHAR(65),
		status TINYINT(1),
		...
	)

	We could create a structure like so:

	type Customer struct {
		Id int64 `customer:"id"`

		Name string `customer:"name"`

		Status bool `customer:"status"`
	}

	// Implement ISchemaEntity
	func (c Customer) GetId() int64 {
		return c.Id
	}

	A DB-aware Model Service type (such as SchemaModelFactory) would
	be able to read the tag metadata, construct SELECT statements
	and create instances of this type.
*/
type ISchemaEntity interface {
	// Allow the no-method "IEntity" interface
	// to be part of the composition of ISchemaEntity
	IEntity

	// Function specifier for retrieving an entity's Id
	GetId() int64
}

/*
	In order to populate data using Go's database/sql
	library, we must be able to scan a sql.Row.

	A working version of a Model Service type will take
	the responsibility of calling ScanRow, passing a *sql.Row.

	To expand on the "customer" idea, let's implement ScanRow:
	(assume we have imported "database/sql")

	func (c *Customer) ScanRow(*sql.Row) error {
		err := row.Scan(&c.Id, &c.Name, &c.Status)
		if err != nil {
			return fmt.Errorf("%v", err)
		}
		return nil
	}
*/

type CanScanSqlRow interface {
	// ScanRow - each entity needs to understand
	// how to "scan" a database 'row'. However, to
	// separate database-specific, the arg is an interface{}.
	// Implementing classes will need to perform type-conversion.
	ScanRow(*sql.Row) error
}

type CanScanSqlRows interface {
	// ScanRows - each entity needs to understand
	// how to "scan" a database 'row'. However, to
	// separate database-specific, the arg is an interface{}.
	// Implementing classes will need to perform type-conversion.
	ScanRows(*sql.Rows) error
}

// ScannableEntities are interfaces that:
// - typically have a single field, an array of single IEntity instances,
// - provide the ability ScanRows of *sql.Rows (database/sql),
// - inform a Factory/Model what type of IEntity it is wrapping (ArrayOf), and
// - return the array of entities (Entities)
//
// Consider the above 'Customer' example, and let's now define a Store.
//
// type GroceryStore struct {
//   Customers []Customer
// }
//
// func (g GroceryStore) ArrayOf {
//   return Customer{}
// }
//
// func (g *GroceryStore) ScanRows() (error) {
//   for rows.Next() {
//     c := &Customer{}
//     err := rows.Scan(&c.Id, &c.Name, &c.Status)
//     if err != nil {
//       return err
//		 }
//     g.Customers = append(g.Customers, *c)  // 'c' is a pointer but we need to get at its element...
//   }
//   return nil
//}
//
//func (g *GroceryStore) Entities() []model.IEntity {
//	data := []model.IEntity{}
//	for _, el := range g.Customers {
//		data = append(data, el)
//	}
//	return data
//}
//
type ScannableEntities interface {
	CanScanSqlRows
	HasArrayOfEntities
}

type ScannableEntity interface {
	ISchemaEntity
	CanScanSqlRow
}
