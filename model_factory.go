package model

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"strings"
)

type HasDatabaseConfig interface {
	GetDatabaseDSN() string
	GetDriver() string
	GetDatabaseHost() string
}

/*
	ISqlModelFactory is the base interface for a service that
	could serve as a basis for all schema-related Models.
*/
type ISqlModelFactory interface {

	// GetEntityById will fetch/retrieve an IEntity instance.
	// Inputs are a "ScannableEntity" instance that will be
	// 'reflected' upon to (likely) determine the columns for selection.
	GetEntityById(ScannableEntity, int64) (IEntity, error)

	// GetAll will fetch/retrieve all entities.
	// Inputs are:
	// - ScannableEntities instance (used for reflection)
	// - page number
	// - items to fetch
	GetAll(ScannableEntities, uint8, uint16) ([]IEntity, error)

	// GetFiltered will fetch/retrieve entities given a filter.
	GetFiltered(ScannableEntities, map[string]interface{}) ([]IEntity, error)

	Save(ISchemaEntity) error
}

// EntityReflection
// Instances of ISqlModelFactory may use this type
// to store 'reflection' information about an entity.
//
// Note the odd naming convention:
// - xxxxxT refers to a relect.Type
// - xxxxxV refers to a reflect.Value
// - xxxxxF refers to a reflect.StructField
type EntityReflection struct {
	// Reflection Type
	SchemaT reflect.Type

	// Schema(table) name of the entity during
	// the process of fetching data.
	TargetSchemaName string

	EntityT reflect.Type

	// Entity Lists are struct types that contain an array of
	// single IEntity-aware structs.
	EntityListT reflect.Type

	EntityV reflect.Value

	SchemaDefF reflect.StructField

	// This field's type is the same as the return type
	// for model.HasArrayOfEntities.ArrayOf() --> interface{}
	TargetEntity interface{}
}

// Implements ISchemaModelFactory
//
// See notes on 'SchemaDef' below on how to construct
// a 'Schema' type
type SqlModelFactory struct {
	// Access to database, please? :)
	DB *sql.DB

	// Working Schema
	// This can be any type consisting of fields that are tagged.
	// Consider this:
	//
	// type MusicSchema struct {
	//     CD CD `schema:"some_cd_table"`
	//     Song Song `schema:"some_songs_table"`
	//     Rating `schema:"some_rating_table"`
	// }
	//
	// Each field in the struct must have a 'schema' tag in order
	// to be recognized by the Model Factory's reflection process.
	SchemaDef interface{}

	ItemsPerPage uint8
}

func NewSqlModelFactory(ctx context.Context, schemaDef interface{}) (*SqlModelFactory, error) {
	var conf HasDatabaseConfig
	if v := ctx.Value("conf"); v != nil {
		// assert that v is indeed a "HasDatabaseConfig" interface
		conf = v.(HasDatabaseConfig)
	}

	// TODO need a model factory call here, so as to
	// remove the "database-driver" dependency from this
	// function

	db, err := sql.Open(conf.GetDriver(), conf.GetDatabaseDSN())
	if err != nil {
		return nil, fmt.Errorf("unable to establish a database connection: %s", err)
	}

	f := &SqlModelFactory{DB: db, SchemaDef: schemaDef, ItemsPerPage: 15}
	return f, nil
}

func (f *SqlModelFactory) GetEntityById(e ScannableEntity, id int64) (IEntity, error) {
	log.Printf("GetEntityById: %s", e)
	_, err := f.getById(e, id)
	if err != nil {
		return nil, fmt.Errorf("GetEntityById error: %s", err)
	}
	log.Printf("GetEntityById: %s", e)
	return e, nil
}

func (f *SqlModelFactory) getById(e ScannableEntity, id int64) (IEntity, error) {
	reflection := f.reflectionFromEntity(e)

	var columns []string
	var hasManyRelates []reflect.StructField
	var belongsToRelates []reflect.StructField

	log.Printf("SqlModelFactory::getById: Number of fields in '%s' = %d\n", reflection.TargetSchemaName, reflection.EntityT.NumField())

	var query strings.Builder
	query.WriteString("SELECT ")

	columns = f.schemaColumnsForEntity(e, reflection.TargetSchemaName)
	query.WriteString(strings.Join(columns, ", "))
	fmt.Fprintf(&query, " FROM %s WHERE id = %d", reflection.TargetSchemaName, id)

	log.Printf("SqlModelFactory::getById query = %s\n", query.String())
	row := f.DB.QueryRowContext(context.Background(), query.String())

	// Type-cast 'e' as "CanScanSqlRow"
	err := e.ScanRow(row)
	if err != nil {
		return nil, fmt.Errorf("error with query: %v", err)
	}

	for i := 0; i < reflection.EntityT.NumField(); i++ {
		structF := reflection.EntityT.Field(i)

		if _, ok := structF.Tag.Lookup("hasmany"); ok {

			// This is the target "Has Many" field within "entity"
			ptr := reflect.New(structF.Type)
			relatedV := reflection.EntityV.Field(i)
			//b := ptr.Elem().Interface()

			if _, ok := ptr.Type().MethodByName("ArrayOf"); ok {
				//relatedEntity := ptr.Interface().(ScannableEntities).ArrayOf()

				filter := make(map[string]interface{})
				colname := f.relatedColNameForEntityField(e, structF.Type.Name())
				filter[colname] = e.GetId()

				log.Printf("SqlModelFactory::getById attempting to call 'getAll' with relatedEntity (from ptr): %s\n", relatedV.Addr().Interface())
				_, err := f.getAll(relatedV.Addr().Interface().(ScannableEntities), 0, 1000, filter)
				if err != nil {
					log.Fatalf("SqlModelFactory::getById FATAL: %s\n", err)
				}
				log.Printf("SqlModelFactory::getById returned from 'getAll' with relatedEntity (from ptr): %s\n", relatedV.Addr().Interface())

				if err != nil {
					return nil, fmt.Errorf("GetEntityById: error in 'hasMany' related query: %v", err)
				}
			}
			hasManyRelates = append(hasManyRelates, structF)
		}
	}
	log.Printf("SqlModelFactory::getById - What '%s' looks like at the moment...\n", e)
	log.Printf("SqlModelFactory::getById - Detected %d 'hasMany' relationship(s)...\n", len(hasManyRelates))

	for i := 0; i < reflection.EntityV.NumField(); i++ {
		//elem := entityV.Field(i)
		relatedF := reflection.EntityT.Field(i)
		if _, ok := relatedF.Tag.Lookup("belongsto"); ok {

			// This is the target "Has Many" field within "entity"
			relatedEntityV := reflection.EntityV.FieldByName(relatedF.Name)

			// Need to reflect on the name of the field used
			// to define the 'schema' relationship.
			// Then, create a Field Value which will be used when
			// scanning the database row later on
			relationFieldTag := relatedF.Tag.Get("relationField")
			relationFieldVal := reflection.EntityV.FieldByName(relationFieldTag)

			// relatedEntityV is the field "value" reflection.
			// However, since our likely data struct implements "ScanRow"
			// and accepts a pointer to the function call, we must first
			// get the memory address (the pointer), then the "interface{}"
			// reprentation of that pointer.
			//
			// relationFieldVal.Interface() represents the live data structure value
			// cast as the generic "interface{}" type, but suitable for typecasting
			_, err := f.getById(
				relatedEntityV.Addr().Interface().(ScannableEntity),
				relationFieldVal.Interface().(int64))

			if err != nil {
				return nil, fmt.Errorf("GetEntityById: error in scanning 'belongsTo' related query: %v", err)
			}

			belongsToRelates = append(belongsToRelates, relatedF)
		}

		// CONCLUSION
		// ----------------------------------------------------------------
		// Yes, this is alot of work/code, but it'll allow other
		// Go authors the simplicity of defining a struct that
		// implements ISchemaEntity, with fields (properties) containing
		// metadata tags (see GenericMessage and/or ExternalLink).
	}

	return e, nil
}

func (f *SqlModelFactory) GetAll(e ScannableEntities, page uint8, itemCount uint16) ([]IEntity, error) {
	_, err := f.getAll(e, page, itemCount, nil)
	if err != nil {
		return nil, fmt.Errorf("SqlModelFactory::GetAll error: %s", err)
	}
	return e.Entities(), nil
}

func (f *SqlModelFactory) getAll(e ScannableEntities,
	page uint8,
	itemCount uint16,
	conditions map[string]interface{}) ([]IEntity, error) {
	reflection := f.reflectionFromEntityList(e)

	var columns []string
	var currentPage uint8 = page
	var itemsPerPage uint16 = itemCount | uint16(f.ItemsPerPage)

	log.Printf("SqlModelFactory::getAll - Number of fields in '%s' = %d\n", reflection.TargetSchemaName, reflection.EntityT.NumField())

	var query strings.Builder
	query.WriteString("SELECT ")

	// collect the column names for the target entity
	columns = f.schemaColumnsForEntity(
		reflection.TargetEntity.(ISchemaEntity),
		reflection.TargetSchemaName)

	query.WriteString(strings.Join(columns, ", "))
	fmt.Fprintf(&query, " FROM %s", reflection.TargetSchemaName)

	var binds []interface{}
	if len(conditions) > 0 {
		var clauses []string
		for k, v := range conditions {
			clauses = append(clauses, fmt.Sprintf("%s = ?", k))
			binds = append(binds, v)
		}
		fmt.Fprintf(&query, " WHERE %s", strings.Join(clauses, " AND "))
	}
	fmt.Fprintf(&query, " LIMIT %d, %d", currentPage, itemsPerPage)

	log.Printf("SqlModelFactory::getAll query is: %s\n", query.String())

	var err error
	var rows *sql.Rows
	rows, err = f.DB.QueryContext(context.Background(), query.String(), binds...)
	if err != nil {
		return nil, fmt.Errorf("%s", err)
	}
	defer rows.Close()
	e.(CanScanSqlRows).ScanRows(rows)

	//var entities []IEntity = e.Entities()
	log.Printf("SqlModelFactory::GetAll - Fetched %d entities of type '%s'...", len(e.Entities()), reflection.EntityT.Name())

	return e.Entities(), nil
}

func (f *SqlModelFactory) GetFiltered(s ScannableEntities, m map[string]interface{}) ([]IEntity, error) {
	return nil, fmt.Errorf("Currently Not Implemented.")
}

func (f *SqlModelFactory) Save(e ISchemaEntity) error {
	return fmt.Errorf("Currently Not Implemented.")
}

// Uses the metadata "tags" from both the entity and the
// Factory's associated 'Schema' to get the column names.
func (s *SqlModelFactory) schemaColumnsForEntity(entity IEntity, schemaName string) []string {
	var columns []string

	//fmt.Printf("GetSchemaColumns called with schemaName = [%s]\n", schemaName)
	entityT := reflect.TypeOf(entity)
	if entityT.Kind() == reflect.Ptr {
		entityT = entityT.Elem()
	}
	for i := 0; i < entityT.NumField(); i++ {
		f := entityT.Field(i)
		// build list of columns (fields tagged by the 'tableName')
		if columnName, ok := f.Tag.Lookup(schemaName); ok {
			columns = append(columns, fmt.Sprintf("%s.%s", schemaName, columnName))
		}
	}
	return columns
}

// Create a "reflection" of the entity and schema
// (prior to producing SQL statements)
func (f *SqlModelFactory) reflectionFromEntity(e IEntity) EntityReflection {
	r := EntityReflection{}
	r.SchemaT = reflect.TypeOf(f.SchemaDef)

	// make sure to account for the possibility that "e" is a pointer
	// instead of a copy
	r.EntityT = reflect.TypeOf(e)
	if r.EntityT.Kind() == reflect.Ptr {
		r.EntityT = r.EntityT.Elem()
	}

	r.EntityV = reflect.ValueOf(e)
	if r.EntityV.Kind() == reflect.Ptr {
		r.EntityV = r.EntityV.Elem()
	}

	r.SchemaDefF, _ = r.SchemaT.FieldByName(r.EntityT.Name())
	r.TargetSchemaName = r.SchemaDefF.Tag.Get("schema")

	return r
}

func (f *SqlModelFactory) reflectionFromEntityList(e HasArrayOfEntities) EntityReflection {
	r := EntityReflection{}
	r.SchemaT = reflect.TypeOf(f.SchemaDef)

	// make sure to account for the possibility that "e" is a pointer
	// instead of a copy
	r.EntityListT = reflect.TypeOf(e)
	if r.EntityListT.Kind() == reflect.Ptr {
		r.EntityListT = r.EntityListT.Elem()
	}

	r.TargetEntity = e.(HasArrayOfEntities).ArrayOf()
	r.EntityT = reflect.TypeOf(r.TargetEntity)

	if r.EntityT.Kind() == reflect.Ptr {
		r.EntityT = r.EntityT.Elem()
	}
	r.SchemaDefF, _ = r.SchemaT.FieldByName(r.EntityT.Name())
	r.TargetSchemaName = r.SchemaDefF.Tag.Get("schema")

	return r
}

func (f *SqlModelFactory) relatedColNameForEntityField(entity interface{}, fieldName string) string {
	entityT := reflect.TypeOf(entity)
	if entityT.Kind() == reflect.Ptr {
		entityT = entityT.Elem()
	}
	fieldF, _ := entityT.FieldByName(fieldName)
	columnName, ok := fieldF.Tag.Lookup("relcolname")
	if !ok {
		panic(fmt.Sprintf("unable to determine column name for field '%s' from entity '%s'.  Please check tags.", fieldName, entityT.Name()))
		return ""
	}
	return columnName
}
